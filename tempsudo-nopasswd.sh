#!/bin/bash

# ======= Functions =========

# -------- Reading the information ----------

reading_info(){
	echo -e "Please enter the Work Order or REQ number:\n"
	read wo

	echo -e "Please follow the description:\nThe -a accounts with , in between (x,y,z...)\nThe groups with a + before group name (+group_name)\n"
	read acc

	echo -e "How long do you need this permission (Please define the number of days. up to 14 days) :\n"
	read length
}

# ------- atd pkg installation ---------

installing_pkg(){
ansible-playbook servers -i inventory rpm_installation.yml
}

# ------ Creating the file(s) and adding the permissions ---------

file_creation(){
	if [[ ("$wo" != "") && ("$wo" != " ") ]]
	then
		touch ./"$wo"
		echo -e "#$wo \n" > ./"$wo"
		echo "$acc ALL=(ALL) NOPASSWD:ALL" >> ./"$wo"	
    else
		echo "The permission hasn't been granted. Please try again."
	fi
}

# ======== Copying the sudoers file ========
copy_file(){
    cp ./copyfile ./copyfile_"$wo"
    sed -i 's/workorder/'$wo'/g' ./copyfile_"$wo".yml
    ansible servers -i inverntory copyfile_"$wo".yml
}

# ----- Adding the job to atd ---------------

at_job(){
	ansible servers -i inventory -m command -a "echo 'rm -f /etc/sudoers.d/$wo' | at now + $length days"
	echo -e "============================= \nHere is the due time: "
	ansible servers -i inventory -m command -a "atq"
	echo -e "\n============================="
}


# ======= Main ===========

reading_info
installing_pkg
file_creation
copy_file
at_job